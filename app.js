var express = require('express'), 
 	//spawn = require("child_process").spawn,
    app = express();
var SerialPort = require("serialport").SerialPort

var runningOnPi = (process.env.USER=="pi")?true:false

// Send an object as a JSON response with HTTP code `200 OK`.
function sendAsJson(obj, res) {
  res.writeHead(200, {'Content-Type': 'application/json'});
  res.write(JSON.stringify(obj));
  res.end();
}

function parseTime(timestring){
	return timestring.split(':').map(Number);
}
function minutesFromMidnight(hours,minutes){
  return hours * 60 + minutes;
}

var serialPort = runningOnPi ? new SerialPort("/dev/ttyACM0", {baudrate: 115200}) :
  null;
 //new SerialPort("/dev/tty.usbmodem12341", {baudrate: 115200});

var alarm = {
  "time": parseTime("16:13"),
  "enabled": false
}

var mainLoopIntervalMs = 1000 * 60
var state = "POLL"
var lastPoll = true;
var intensity = 0;

function resetAlarm(){
  state = "POLL"
  lastPoll = true;
  mainLoopIntervalMs = 1000 * 60;
}

resetAlarm();
function mainLoop (){
  var now = new Date();
  console.log("Time now: " + now.getHours() + ':' + now.getMinutes() + "  Alarm time: " + alarm.time[0] + ':' + alarm.time[1] + "   State: "+state+"   lastPoll: "+lastPoll+"    intensity: "+intensity);

  if(state == "POLL"){
    //console.log("Time now: " + now.getHours() + ':' + now.getMinutes());
    var minutesFromMidnight = now.getHours() * 60 + now.getMinutes();
    var alarmMinutesFromMidnight = alarm.time[0] * 60 + alarm.time[1];

    if (minutesFromMidnight >= alarmMinutesFromMidnight) {
      if(lastPoll == false){
        state = "ALARM_RAMP";
        intensity = 0;
      }
      lastPoll = true;
    }else{
      lastPoll = false;
    }
  }else if(state == "ALARM_RAMP"){
    if(intensity >= 256){
      state = "ALARM_FULL";
      intensity = 256;
    }else{
      writeIntensity(1,intensity,serialPort, function(message,result){
        console.log(message +" "+ result);
        intensity = intensity+7;
      }); 
    }
  }else if(state == "ALARM_FULL"){
    //Don't do anything in particular
    //For now just reset the alarm so it is ready for tomorrow
    resetAlarm();
  }else{
    console.log("Unknown state reached!");
    state = "POLL";
  }

  setTimeout(mainLoop, mainLoopIntervalMs ); //every minute
}


function beginAlarm(){
    setInterval(function () {
      console.log("Please wake up");
    }, 1000);
}

var last_instensity_written = 0;
function writeIntensity(channel, value, serialport, callback){
  if(channel <= 0 || channel > 512) {callback("error","Invalid channel. Must be between 1 and 512"); return;}
  if(value < 0 || value > 255) {callback("error","Invalid value. Must be between 0 and 255"); return;}
  last_instensity_written = value;
  var data = channel.toString() + 'c' + value.toString() + 'w';
  if(serialport != null){
    serialport.write(data, function(error){
      if(error){callback("error","Failed to write over serial. data:"+data);}
      else {callback("success",data);}
    })
  }else{
    callback("error","Serial port is not initialized");
  }
}


app.use(express.static(__dirname + '/static'));

// Allow Express to parse POST requests
app.use(express.bodyParser());


app.get('/', function (req, res) {
    //res.render("home.jade");
    res.sendfile(__dirname + "/views/home.html");
});


app.post('/', function (req, res) {
  time = parseTime(req.body.date);
  resetAlarm();
  console.log("Wake up time changed: "+req.body.date);
  res.end();
});

app.get('/alarm', function (req, res) {
  if(alarm.enabled == true){
    sendAsJson({alarm_time: alarm.time[0] + ':' + alarm.time[1]}, res);
  }else{
    sendAsJson({alarm_time: ''}, res);
  }
});

app.post('/alarm', function (req, res) {
  if(req.body.alarm_enable == 'false'){
    alarm.enabled = false;
    console.log("Alarm disabled");
  }else{
    alarm.enabled = true;
    alarm.time = parseTime(req.body.alarm_time);
    console.log("Alarm enabled: "+req.body.alarm_time);
  }
  res.end();
});

app.get('/lights', function (req, res) {
    sendAsJson({light_intensity_byte: last_instensity_written}, res);
});

app.post('/lights', function (req, res) {
  writeIntensity(1, req.body.light_intensity_byte, serialPort, function(msg, rsp){
    if(msg == "success"){console.log("Light intensity changed: " + rsp);}
  })
  res.end();
});


app.listen(3000);
 
mainLoop();